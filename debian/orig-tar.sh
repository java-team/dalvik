#!/bin/bash

#######################################################
# This script fetches the latest commit of the branch #
# with the specified revision.                        #
#######################################################

if [ -d dalvik -o -f dalvik ]
then
    echo "The file/directory 'dalvik' exists .. aborting."
    exit
fi

REVISION=20

git clone -b tools_r$REVISION https://android.googlesource.com/platform/dalvik
VERSION=`git --git-dir=dalvik/.git log -1 --format=%cd~%h --date=short | sed s/-//g`
echo "Version "$VERSION
echo "Deleting not needed files ..."
rm -fr `find dalvik -maxdepth 1 -type d ! -name dalvik ! -name dx`
rm -fr dalvik/.git
rm -fr `find dalvik/dx -maxdepth 1 -type d ! -name dx ! -name src`
rm -fr dalvik/dx/.project dalvik/dx/.classpath

echo "Packging archive into ../dalvik_$REVISION+git$VERSION.orig.tar.gz ..."
tar -czf ../dalvik_$REVISION+git$VERSION.orig.tar.gz dalvik
echo "Deleting folder 'dalvik'"
rm -Ir dalvik
